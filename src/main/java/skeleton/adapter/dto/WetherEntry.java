package skeleton.adapter.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by domin on 22/05/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WetherEntry {
        private Integer id;
        private String name;
        private Integer cod;
        //private SysEntry sys;
        private Integer dt;
        //private Clouds all;
        @JsonProperty("weather")
        private List<WeatherEntry> wh;
        private MainEntry main;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCod() {
        return cod;
    }

    public void setCod(Integer cod) {
        this.cod = cod;
    }

    public Integer getDt() {
        return dt;
    }

    public void setDt(Integer dt) {
        this.dt = dt;
    }

    public List<WeatherEntry> getWh() {
        return wh;
    }

    public void setWh(List<WeatherEntry> wh) {
        this.wh = wh;
    }

    public MainEntry getMain() {
        return main;
    }

    public void setMain(MainEntry main) {
        this.main = main;
    }

    @Override
    public String toString() {
        return "WetherEntry{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cod=" + cod +
                ", dt=" + dt +
                ", wh=" + wh +
                ", main=" + main +
                '}';
    }
}

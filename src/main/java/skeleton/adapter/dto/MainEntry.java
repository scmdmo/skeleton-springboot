package skeleton.adapter.dto;

/**
 * Created by domin on 22/05/17.
 */
public class MainEntry {

    private Float temp;

    public Float getTemp() {
        return temp;
    }

    public void setTemp(Float temp) {
        this.temp = temp;
    }

    @Override
    public String toString() {
        return "MainEntry{" +
                "temp=" + temp +
                '}';
    }
}

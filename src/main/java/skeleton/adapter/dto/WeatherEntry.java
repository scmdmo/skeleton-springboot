package skeleton.adapter.dto;

/**
 * Created by domin on 22/05/17.
 */
public class WeatherEntry {
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "WeatherEntry{" +
                "description='" + description + '\'' +
                '}';
    }
}

package skeleton.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import skeleton.adapter.WetherAPI;
import skeleton.adapter.dto.WetherEntry;

/**
 * Created by domin on 22/05/17.
 */
@Component
public class WetherAPIImpl implements WetherAPI {

    //@Autowired
    private RestTemplate restTemplate;
    private String apiKey = "ad558362faeb6c8bae88dce6fa433b15";
    private String urlRoot = "http://api.openweathermap.org/data/2.5/weather?appid=%s&mode=json&lang=en&units=metric&q=%s";  //TODO, volcar a configuracion

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public void setUrlRoot(String urlRoot) {
        this.urlRoot = urlRoot;
    }

    public WetherAPIImpl() {
        this.restTemplate = new RestTemplate(); //TODO move to beans
    }

    //TODO move to application context..  restTempleta as bean
    /*@Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        // Do any additional configuration here
        return builder.build();
    }*/

    @Override
    public WetherEntry getEntry(String city) {
        String url = String.format(urlRoot, apiKey, city);
        ResponseEntity<WetherEntry> entity = restTemplate.getForEntity(url, WetherEntry.class);
        System.out.print("Status: "+entity.getStatusCode()+" ");//TODO usar clase de log
        return entity.getBody();
    }
}

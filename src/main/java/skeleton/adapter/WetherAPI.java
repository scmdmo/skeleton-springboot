package skeleton.adapter;

import skeleton.adapter.dto.WetherEntry;

/**
 * Created by domin on 22/05/17.
 */
public interface WetherAPI {
    WetherEntry getEntry(String city);
}

package skeleton;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import skeleton.adapter.dto.WetherEntry;
import skeleton.business.WeatherBusiness;
import skeleton.business.impl.WeatherBusinessImpl;
import skeleton.service.WheaterService;
import skeleton.service.impl.WeatherServiceImpl;

/**
 * Created by domin on 22/05/17.
 */
//@Configuration
public class AppConfig {

  //  @Bean
    public WheaterService wheaterService() {
        return new WeatherServiceImpl();
    }

//    @Bean
    public WeatherBusiness weatherBusiness() {
        return new WeatherBusinessImpl();
    }

    // ...
}

package skeleton.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import skeleton.adapter.dto.WetherEntry;
import skeleton.service.WheaterService;

/**
 * Created by domin on 22/05/17.
 */
@RestController
public class WeatherController {

    @Autowired
    private WheaterService wheaterService;

    public void setWheaterService(WheaterService wheaterService) {
        this.wheaterService = wheaterService;
    }

    @RequestMapping(value ="/wh/{city}", method = RequestMethod.GET)
    public @ResponseBody WetherEntry getWeather(@PathVariable String city){
        return wheaterService.getWeather(city);
    }
}

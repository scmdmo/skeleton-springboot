package skeleton.controller;

/**
 * Created by domingo on 22/05/17.
 */
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

    @RequestMapping("/hello")
    public String index() {
        return "Greetings from skeleton proyect with Spring Boot!";
    }

}

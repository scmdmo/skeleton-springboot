package skeleton.data.dto;

/**
 * Created by domin on 22/05/17.
 */
public class Row {
    private int id;
    private String somecolumn;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSomecolumn() {
        return somecolumn;
    }

    public void setSomecolumn(String somecolumn) {
        this.somecolumn = somecolumn;
    }
}

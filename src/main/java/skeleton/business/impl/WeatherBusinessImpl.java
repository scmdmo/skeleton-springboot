package skeleton.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import skeleton.adapter.WetherAPI;
import skeleton.adapter.dto.WetherEntry;
import skeleton.business.WeatherBusiness;

/**
 * Created by domin on 22/05/17.
 */
@Component
public class WeatherBusinessImpl implements WeatherBusiness {

    @Autowired
    private WetherAPI wetherAPI;

    public void setWetherAPI(WetherAPI wetherAPI) {
        this.wetherAPI = wetherAPI;
    }

    @Override
    public WetherEntry getWeather(String city) {
        return wetherAPI.getEntry(city);
    }
}

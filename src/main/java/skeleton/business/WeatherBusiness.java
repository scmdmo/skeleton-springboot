package skeleton.business;

import skeleton.adapter.dto.WetherEntry;

/**
 * Created by domin on 22/05/17.
 */
public interface WeatherBusiness {
    WetherEntry getWeather(String city);
}

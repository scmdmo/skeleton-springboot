package skeleton.service;

import skeleton.adapter.dto.WetherEntry;

/**
 * Created by domin on 22/05/17.
 */
public interface WheaterService {
    WetherEntry getWeather(String city);
}

package skeleton.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import skeleton.adapter.dto.WetherEntry;
import skeleton.business.WeatherBusiness;
import skeleton.service.WheaterService;

/**
 * Created by domin on 22/05/17.
 */

@Service
public class WeatherServiceImpl implements WheaterService {
    @Autowired
    private WeatherBusiness weatherBussines;

    public void setWeatherBussines(WeatherBusiness weatherBussines) {
        this.weatherBussines = weatherBussines;
    }

    @Override
    public WetherEntry getWeather(String city) {
        return weatherBussines.getWeather(city);
    }
}

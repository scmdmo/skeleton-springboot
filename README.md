Description:
Write a small web application to display the weather for a given city. The application also has to remember/persist each city consulted and be able to display the last 5 queried.

Weather API to use:
Rest web service API URL: http://api.openweathermap.org/data/2.5/weather?appid={key}&mode=json&lang=en&units=metric&q={city}

Key: ad558362faeb6c8bae88dce6fa433b15

Technologies to use:
  - Spring Framework MVC
  - Spring Framework DAO (JPA or Hibernate or MyBatis)
  - MySQL or PostgreSQL or Oracle XE as DB
  - Choose any exising framework for REST integration (i.e. Spring RestTemplate)

